#!/bin/sh
#
# Copyright (c) 2020  S2 Factory, Inc.  All rights reserved.

image="$1"
shift

for tag in "$@"; do
	docker pull $image:$tag > .log-$tag
	docker image inspect $image:$tag | jq -r '.[0].Id' > .id-$tag
done

# donebit Variables:
# sh-basic-offset: 8
# sh-indentation: 8
# End:
