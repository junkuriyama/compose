
image:
	docker build -t kuriyama/docker-compose:19.03 -f Dockerfile.19.03 .
	docker build -t kuriyama/docker-compose:20.10 -f Dockerfile.20.10 .

pull:
	@./check.sh docker 19.03
	@./check.sh docker 20.10

check:
	@if git status -s | grep -q M; then\
		git add .id-* && git commit -m "Update IDs."; git push;\
	fi
